<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Support\DataTablePaginate;

class Customer extends Model
{
    use DataTablePaginate;
    protected $fillable = ['name', 'email', 'address', 'phone'];
    protected $hidden = [];
}
