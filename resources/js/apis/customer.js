import {apiRequest} from "../helpers/apiRequest";
import {apiMethod} from "../constants/apiMethod";


export const showCustomer = (id) => {
    return new Promise(
        (resolve, reject) => {
            apiRequest('/api/customer/' + id)
                .then(res => resolve(res))
                .catch(function (error) {
                    reject(error)
                })
        }
    )
}

export const deleteCustomer = (id) => {
    return new Promise(
        (resolve, reject) => {
            apiRequest('/api/customer/' + id, apiMethod.delete)
                .then(res => resolve(res))
                .catch(function (error) {
                    reject(error)
                })
        }
    )
}