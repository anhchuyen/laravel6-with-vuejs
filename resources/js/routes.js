import CustomerList from './components/customer/ListComponent';
import InvoiceList from './components/invoice/ListComponent';
import CustomerShow from './components/customer/ShowComponent';

const routes = [
    {path:'/invoice', name: 'invoiceList', component: InvoiceList},
    {path: '/customer', name: 'customerList', component: CustomerList},
    {path: '/customer/show/:id', name: 'customerShow', component: CustomerShow}
];

export default routes;